# capitolo 5
# if and else

''' 1 '''
money = 501
if money > 1000:
		print("I'm rich!")
else:
	print("I'm not rich..")
	print("..but I might be later!")
	
''' 2 '''
twinkies = 300
if twinkies <100 or twinkies >500:
		print("Too few or too many Twinkies")
else:
	print("Had enough")

''' 3 '''
# btw 100 and 500  OR btw 1000 and 5000
if money >=100 and money <=500:
	print("btw 100 and 500")
elif money >=1000 and money <=5000:
	print("btw 1000 and 5000")
else:
	print("Not between 100-500 nor 1000-5000")

''' 4 '''
ninjas = 21
if ninjas < 10:
	print("I can fight those ninjas")
elif ninjas < 30:
	print("It'll be a struggle but I can take 'em")
elif ninjas < 50:
	print("That's too many")
	
