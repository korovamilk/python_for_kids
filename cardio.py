#!/usr/bin/python
''' cardio.py
	given age, does simple cardio freq calculations
	+ this is an almost useless exercise + '''

def new_line():
	print(57 * '_')
		
def cardio(my_age):
	print('\n Here is the suggested cardio freqs for the age of %s...\n' % age)
	# 65%	85%	optimal: 70%
	fc_max = (220 - my_age)
	max_aero_fc = fc_max * 0.85
	opt_aero_fc = fc_max * 0.70
	min_aero_fc = fc_max * 0.65
	
	print('         Max Cardio Freq: %s bpm\t\t[85*100]' % max_aero_fc)
	print(" *** Optimal Cardio Freq: %s bpm ***\t\t[70*100]" % opt_aero_fc)
	print('         Min Cardio Freq: %s bpm\t\t[65*100]' % min_aero_fc)

new_line()
age = raw_input('\n  Enter your age: ')
age = int(age)
new_line()
cardio(age)
new_line()
