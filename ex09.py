#!/usr/bin/env python
print('#1: Mystery code')
a = abs(10) + abs(-10)
print(a) # 20
b = abs(-10) + -10
print(b) # 0

print('\n#2: A Hidden Message')
message = '''this if is you not are a reading very this good then way you to have
hide done a it message wrong'''

print(message)


# print(dir(splitted_msg))
splitted_msg = message.split()
print(splitted_msg)
for x in range(0, len(splitted_msg), 2):
	print(splitted_msg[x])


### extra ## writing message to file
write_message = open('./workdir/message.txt', 'a') # using non-destructive append mode
write_message.write(message)
write_message.close()

print('\n#3: Copying a file')
my_file = open('./workdir/message.txt', 'r')
text = my_file.read()
my_file.close()
print('\n***Content of message.txt: \n')
print(text)

print('\n*** Copying to new_message.txt')
new_file = open('./workdir/new_message.txt', 'a') # using non-destructive append mode
new_file.write(text)
new_file.close()
print('Done')
print('\n*** Content of new_message.txt:')
new_file = open('./workdir/new_message.txt', 'r')
text_new = new_file.read()
new_file.close()
print(text_new)


## - OR - (clearer)
# print('\n#3BIS: Copying a file')
# f = open('test.txt')
# s = f.read()
# f.close()
# f = open('output.txt', 'w')
# f.write(s)
# f.close()

## - OR - (faster)
## we may use a Python module called shutil:
# import shutil
# shutil.copy('test.txt', 'output.txt')
