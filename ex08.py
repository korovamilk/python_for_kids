#!/usr/bin/env python
print('#1: The Giraffe Shuffle')

class Giraffes():
	def right_Foot_Forward(self):
		print('right foot forward')
	def left_Foot_Forward(self):
		print('left foot forward')
	def right_Foot_Backward(self):
		print('right foot back')
	def left_Foot_Backward(self):
		print('left foot back')

	def dance(self):
		print("\nLet's Dance! %s \n" % self)
		self.left_Foot_Forward()
		self.left_Foot_Backward()
		self.right_Foot_Forward()
		self.right_Foot_Backward()
		self.left_Foot_Backward()
		self.right_Foot_Backward()
		self.right_Foot_Forward()
		self.left_Foot_Forward()
		

reginald = Giraffes()
reginald.dance()
print('End.')

print('#2: Turtle Pitchfork')
import turtle
first = turtle.Pen()
second = turtle.Pen()
third = turtle.Pen()
fourth = turtle.Pen()

first.forward(100)
first.left(90)
first.forward(20)
first.right(90)
first.forward(20)

second.forward(100)
second.right(90)
second.forward(20)
second.left(90)
second.forward(20)

third.forward(80)
third.left(90)
third.forward(40)
third.right(90)
third.forward(60)

fourth.forward(80)
fourth.right(90)
fourth.forward(40)
fourth.left(90)
fourth.forward(60)

fifth = turtle.Pen()
fifth.forward(200)

'''
print('\n### EXTRA: using classes')  ## to be implemented

class Pitchfork():
	
	def __init__(self,side):
		self.forkside = side
	def move_object(self):
		self.forward(80)
		#self.forkside(90)
		self.forward(40)
		#self.forkside(90)
		self.forward(60)

sixth = Pitchfork(left)
sixth.move()
'''
