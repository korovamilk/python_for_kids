#!/usr/bin/env python
import turtle
t = turtle.Pen()

''' boring
# old, tedious way to draw a square:
t.forward(50)
t.left(90)
t.forward(50)
t.left(90)
t.forward(50)
t.left(90)
t.forward(50)
'''

# square loop
t.reset()
for loop in range(1, 5): # 4 times loop
	t.forward(50)
	t.left(90)

# eight-point star
t.reset()
for x in range(1, 9): # 8 times loop
	t.forward(100)
	t.left(225)

# a circle star
t.reset()
for loop in range(1, 38):
	t.forward(100)
	t.left(175)

# spiraling star
t.reset()
for loop in range(1, 20):
	t.forward(100)
	t.left(95)

# big star 
t.reset()
for loop in range(1,19):
	t.forward(100)
	if loop % 2 == 0: # modulo operandi
		t.left(175)
	else:
		t.left(225)
