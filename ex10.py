#!/usr/bin/env python
print('#1: Copied cars')

import copy
class Car:
	pass
	
car1 = Car()
car1.wheels = 4
car2 = car1 # same object == Car()
car2.wheels = 3 # we assign new value to object: Car() == 3
print('car1 = Car()')
print(car1.wheels) # Car() == car2 == 3

car3 = copy.copy(car1) # we copy properties
car3.wheels = 6 
print('car1')
print(car1.wheels) # == 3 - no influence to object this time
print('car3')
print(car3.wheels) # == 6


print('\n#2: Pickled favorites') 
import pickle

favorites = ['Pizza', 'Beer', 'Books', 'Python']
# use pickle to save them in ./workdir/favorites.dat
saved_favs = open(('./workdir/favorites.dat', 'wb')
pickle.dump(favorites, saved_favs)
saved_favs.close()

load_favs = open('./workdir/favorites.dat', 'rb')
read_favs = pickle.load(load_favs)
print(read_favs)
load_favs.close()
