#!/usr/bin/env python
# 11-2 Drawing Figures
import turtle

t = turtle.Pen()
## Drawing a Purple Car
t.color(1,0,1)
t.begin_fill()
t.forward(100)
t.left(90)
t.forward(20)
t.left(90)
t.forward(20)
t.right(90)
t.forward(20)
t.left(90)
t.forward(60)
t.left(90)
t.forward(20)
t.right(90)
t.forward(20)
t.left(90)
t.forward(20)
t.end_fill()
# first wheel
t.color(0,0,0)
t.up()
t.forward(10)
t.down()
t.begin_fill()
t.circle(10)
t.end_fill()
# second wheel
t.setheading(0) # turn turtle face to direction 0 degrees
t.up()
t.forward(90)
t.right(90)
t.forward(10)
t.setheading(0)
t.begin_fill()
t.down()
t.circle(10)
t.end_fill()
# leave canvas
t.up()
t.forward(500)
t.down()
t.reset()
'''
import time 
time.sleep(5)
'''

## Drawing a circle
def my_circle(red, green, blue):
	t.color(red, green, blue)
	t.begin_fill()
	t.circle(50)
	t.end_fill
	t.up()
	
my_circle(0, 1, 0) # yellow circle
my_circle(0, 0.5, 0) # dark green
my_circle(1, 0, 0)
my_circle(0.5, 0, 0)
my_circle(0, 0, 1)
my_circle(0, 0, 0.5)
my_circle(1, 1, 1) # white
my_circle(0, 0, 0) # black
my_circle(0.9, 0.75, 0) # gold
my_circle(1, 0.7, 0.75) # light pink


## Drawing Squares
def mysquare(size):
	for x in range(1, 5):
		t.forward(size)
		t.left(90)

t.reset()
mysquare(25)
mysquare(50)
mysquare(75)
mysquare(100)
mysquare(125)


## Drawing Filled Squares
def mysquare(size, filled):
	if filled == True:
		t.begin_fill()
	for x in range(1, 5):
		t.forward(size)
		t.left(90)
	if filled == True:
		t.end_fill()

t.reset()
mysquare(50, True)
mysquare(150, False)

## Drawing Filled Stars
t.reset()
def mystar(size,filled):
	if filled == True:
		t.begin_fill()
	for x in range(1, 19):
		t.forward(100)
		if x % 2 == 0:
			t.left(175)
		else:
			t.left(225)
	if filled == True:
		t.end_fill()
t.color(0.9, 0.75, 0)
mystar(120, True)
t.color(0, 0, 0)
mystar(120, False)
