# python for kids
# capitolo 4 - disegnare con il modulo turtle

# import del modulo turtle
import turtle

# inizializzazione canvas
t = turtle.Pen()

t.forward(50)
t.left(90)
t.forward(50)
t.left(90)
t.forward(50)
t.left(90)
t.forward(50)
t.left(90)
t.up()
t.forward(200)
t.down()
t.forward(50)
t.left(90)
t.forward(50)
t.left(90)
t.forward(50)
t.left(90)
t.forward(50)
t.left(90)

# cancello canvas e torno al punto di partenza
t.reset()
# per cancellare rimanendo nello stesso punto:
#t.clear()

# disegna due linee parallele
t.forward(50)
t.forward(50)
t.forward(50)
t.up()
t.right(90)
t.forward(10)
t.forward(10)
t.forward(10)
t.right(90)
t.down()
t.forward(30)
t.forward(120)
