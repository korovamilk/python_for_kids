# using functions
print(list(range(0,100)))

def my_test_function(my_name):
	print('\nHello %s!' % my_name)

my_test_function('Mary Jane')

for my_name in "Marco", "Chiara", "No One":
	my_test_function(my_name)

def name_surname(fname, lname):
	print('\nHello %s %s!' % (fname, lname))

name_surname('Mario', 'Rossi')

name = 'Jack'
surname = 'The Ripper'
name_surname(name, surname)

def savings(pocket_money, paper_route, spending):
	return pocket_money + paper_route - spending

print(savings(10, 10, 5))




print('Variables and Scope\n')


# outside var can be used inside a function
a_variable = 100
def variable_test():
	first_var = 10
	second_var = 20
	return first_var * second_var * a_variable

print('Variable_Test: %s' % variable_test())

# two cans a week
def spaceship_building(cans):
	total_cans = 0
	for week in range(1,53):
		total_cans += cans
		print('Week %s = %s cans' % (week, total_cans))

spaceship_building(13)
###########################################

print('\n Modules\n')

import time 
print('Current Time: %s' % time.asctime())

import sys
print("Type some words and then hit enter")
print(sys.stdin.readline())

print("\ncool! ...now, Enter your age: ")
input_age = sys.stdin.readline()
print("\nLa tua eta' e': %s " % input_age)

def silly_age_joke(some_age):
	some_age = int(some_age)
	if some_age >= 10 and some_age <= 29:
		print('What is 13 + 49 + 84 + 155 + 97? A headache!')
	else:
		print('Huh?!')

silly_age_joke(input_age)




