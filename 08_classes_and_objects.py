#!/usr/bin/env python
# classes and objects
class Things:
	pass
	
class Inanimate(Things):
	pass

class Animate(Things):
	pass

# Things is parent of both classes

class Sidewalks(Inanimate):
	pass

class Animals(Animate):
	pass 

class Mammals(Animals):
	pass

class Giraffes(Mammals):
	pass

# instance of class Giraffes ~ our first object
reginald = Giraffes()

def this_is_a_normal_function(): # not associated with a class
	print('I am a normal function')

class ThisIsMySillyClass:
	def this_is_a_class_function():
		print('I am a class function')
	def this_is_also_a_class_function():
		print('I am also a class function. See?')

# adding characteristics
class Animals(Animate):
	def breathe(self): # self: allows a function to call another function
		print('breating')
	def move(self):
		print('moving')
	def eat_food(self):
		print('eating')

class Mammals(Animals):
	def feed_young_with_milk(self):
		print('feeding young')

class Giraffes(Mammals):
	def eat_leaves_from_trees(self):
		print('eating leaves')
		
print('Creating an istance of Giraffes (Reginald)')
reginald = Giraffes()
reginald.move()	# function inherited by Animals class
reginald.eat_leaves_from_trees()
print('')
print('Creating an istance of Giraffes (Harold)')
harold = Giraffes()
harold.move()

print('\nTaking a more graphical approach')

import turtle
avery = turtle.Pen() # we create a turtle object of Pen class
kate = turtle.Pen() # we create an instance of Pen class

# Pen is class
# turtle is a module
# avery and kate are objects of turtle, and members of Pen class

avery.forward(50)
avery.right(90)
avery.forward(20)

kate.left(90)
kate.forward(100)

# let's create another INSTANCE of the Pen CLASS: jakob
jakob = turtle.Pen()
jakob.left(180)
jakob.forward(80)

kate = turtle.Pen() # a new object with same name
kate.forward(90)
kate.left(45)
kate.forward(100)

print('\nEnhancing Giraffes class...')
class Giraffes(Mammals):
	def find_food(self):
		self.move()
		print("I've found food!")
		self.eat_food()
		
	def eat_leaves_from_trees(self):
		self.eat_food()
	def dance_a_jig(self):
		self.move()
		self.move()
		self.move()
		self.move()
		
print("Let's create a new instance of the Giraffes class: Penny ")
penny = Giraffes()
penny.dance_a_jig()
penny.find_food()


# set INIT properties in object 
class Giraffes:
	def __init__(self, spots):
		self.giraffe_spots = spots # property
		## NOTE: when we refer to an object inside the class,
		##       we refer to it as 'self'
ozwald = Giraffes(100)
gertrude = Giraffes(150)

print('\nozwald spots:')
print(ozwald.giraffe_spots) # initialitazion function in action
print('gertrude spots:')
print(gertrude.giraffe_spots) # initialitazion function in action
