# python for kids
# chap 3
print("Wizard List: stringa")
stringa = 'latte, pane, insalata, uova, bretzel'
print(stringa)
print
print("Wizard List: lista")
lista = ['pane','burro','marmellata','acqua','olio']
print(lista)
print
secondo_elemento = lista[1] # si inizia a contare dalla posizione 0
print(secondo_elemento)
# si puo' scrivere anche cosi':
print(lista[1])
print

# switch element
lista[1]= 'margarina'
print(lista)
print

# show subset - mostra dal secondo elemento al quinto (non incluso)
print(lista[1:5])

altra_lista = ['zero', 1, 'due', 'tre', 'quarto elemento']
new_list = [lista, altra_lista]
print(altra_lista)
print(new_list) 

# append di altro elemento
altra_lista.append('elemento estraneo')
print(altra_lista)

altra_lista.append('un altro elemento in append')
print(altra_lista)

# eliminazione di un elemento
del altra_lista[5]
print(altra_lista)

#list arithmetic
list1 = [1, 2, 3, 4]
list2 = ['Io', 'ho', 'molto', 'sonno']
list3 = list1 + list2
print(list3)

# multiply list (scrive list1 x 4 volte)
print(list1 * 4)
# vale anche per le stringhe di testo
print(list2 * 4)
print
# tuples
print('my first Tuple');

tupla = (0, 1, 1, 2, 3)
print(tupla[3])
# anche questo e' un commento! ->
''' non si possono modificare
	i campi della tupla! '''
print(tupla[4])
''' tupla[4] = 1 '''
# TypeError: 'tuple' object does not support item assignment
''' ottimo, se mi serve una lista immodificabile, uso la tupla 
	(ossia una lista, ma con le parentesi tonde) '''

# Python Maps -> liste key: value
favorite_hobbies = {'Tom' : 'Running',
					'Peter' : 'Philosophy',
					'Berthold' : 'Ski'}

print(favorite_hobbies['Tom'])
del favorite_hobbies['Peter']
print(favorite_hobbies)
print(favorite_hobbies['Berthold'])
# replace del value per la key 'Tom'
favorite_hobbies['Tom'] = 'Badminton'
print(favorite_hobbies)
