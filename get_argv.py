#!/usr/bin/env python
import sys

indir = sys.argv[1]
outdir = sys.argv[2]
other = sys.argv[3]
if len(sys.argv) > 4:
	languages = sys.argv[4]
else:
	languages = None

print("Indir: %s ~ Outdir: %s ~ Other: %s ~ Languages: %s" % (indir, outdir, other, languages))
