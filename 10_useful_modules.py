#!/usr/bin/env python

# making copies with copy module
print('#copy')
import copy

class Animal:
	def __init__(self, species, number_of_legs, color):
		self.species = species
		self.number_of_legs = number_of_legs
		self.color = color

harry = Animal('hippogriff', 6, 'pink')

# make copy of a class object
harriet = copy.copy(harry)
print(harry.species)
print(harriet.species)

carrie = Animal('chimera', 4, 'green polka dots')
billy = Animal('bogill', 0, 'paisley')
my_animals = [harry, harriet, carrie, billy]
print(my_animals[2].species)
# make a copy of a list of objects  #### shallow copy 
more_animals = copy.copy(my_animals)
print(more_animals[2].species)

# changes to my_animals list affect also objects present on the other list
my_animals[2].species = 'ghoul'
print(my_animals[2].species)

print(more_animals[2].species)

# but not new elements
sally = Animal('sphinx', 4, 'sand')
my_animals.append(sally)
print(len(my_animals))
print(len(more_animals))

# deep copy
print(my_animals[0].species)
more_animals = copy.deepcopy(my_animals)
my_animals[0].species = 'wyrm'
print(my_animals[0].species)
print(more_animals[0].species)

# keywords
print('\n#keyword')
import keyword
print(keyword.iskeyword('if'))
print(keyword.iskeyword('ozwald'))
print(keyword.kwlist)

# random numbers
import random
print(random.randint(1, 100))
print(random.randint(100, 1000))
print(random.randint(1000, 10000))

# annoying guessing game
print('\n#random.randint')
num = random.randint(1, 10)
while True:
	print('Guess a number between 1 and 10 (** CHEAT: %s **)' % num)
	guess = input()
	i = int(guess)
	if i == num:
		print('You guessed right! Number was: %s' % num)
		break
	elif i < num:
		print('Try higher')
	elif i > num:
		print('Try lower')

print('\n#random.choice')
import random
desserts = ['ice cream', 'pancakes', 'brownies', 'cookies', 'candy']
print(desserts)
print(random.choice(desserts))

print('\n#random.shuffle')
random.shuffle(desserts)
print(desserts)

print('\n#sys')
import sys
print('Write something [max 5 letters]: ')
v = sys.stdin.readline(5)
print("Your words: %s " % v)

print('\n#sys')
sys.stdout.write("What does a fish say when it swims into a wall? Dam.\n")
print('\nPython Version in use:')
print(sys.version)

print('\n#about time')

import time


def lots_of_number(max):
	t1 = time.time()
	for x in range(0, max):
		print(x)
	t2 = time.time()
	print('it tooks %s seconds to write from 0 to %s' % ((t2-t1), max))

lots_of_number(1000)

import sys
import time 

print(time.time())
print(time.asctime())

t = (2020, 2, 23, 10, 30, 48, 6, 0, 0)
print(time.asctime(t))

t = time.localtime()
year = t[0]
print(year)
month = t[1]
print(month)


for x in range(1, 5):
	print(x)
	time.sleep(1)


print('\n#pickle')

import pickle
game_data = {
	'player-position' : 'N23 E45',
	'pockets' : ['keys', 'pocket knife', 'polished stone'],
	'backpack' : ['rope', 'hammer', 'apple'],
	'money' : 158.50 
}

save_file = open('save.dat', 'wb')
pickle.dump(game_data, save_file)
save_file.close()

load_file = open('save.dat', 'rb')
loaded_game_data = pickle.load(load_file)
load_file.close()
print(loaded_game_data)


print('\n#### Calling sys.exit status: 222(!)')
sys.exit(222)
