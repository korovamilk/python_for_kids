#!/usr/bn/env python
'''
from tkinter import *
tk = Tk()
canvas = Canvas(tk, width=600, height=500)
canvas.pack()
canvas.create_polygon(10, 10, 100, 10, 100, 110, fill="", outline="black")

canvas.create_polygon(200, 10, 240, 30, 120, 80, 140, 110, fill="", outline="black")

# displaying text
canvas.create_text(150, 120, text='There once was a man from Toulouse,')
canvas.create_text(130, 140, text='Who rode around on a moose.', fill='red')

canvas.create_text(150, 170, text='He said, "It\'s my curse,', font=('Times', 15))
canvas.create_text(200, 220, text='But it could be worse', font=('Helvetica', 20))
canvas.create_text(220, 270, text='My cousin rides round', font=('Courier', 22))
canvas.create_text(220, 320, text='on a goose.', font=('Courier', 30))

# display images
my_image = PhotoImage(file='./workdir/gb.gif')
canvas.create_image(380, 313, anchor=NW, image=my_image)
# canvas.create_image(x, y, ... 
#   ----------< x
#   |
#   |
#   |
#   |
#   ^ y
'''
# creating basic animation
import time
from tkinter import *
tk_new = Tk()
tk_new.title("Moving Triangle")
canvas_new = Canvas(tk_new, width=600, height=600)
canvas_new.pack()
my_triangle = canvas_new.create_polygon(10, 10, 10, 60, 50, 35, fill="red")
# handling events
def move_triangle(event):
	if event.keysym == 'Up':
		canvas_new.move(1, 0, -30)
		canvas_new.itemconfig(1, fill="blue", outline="red")
	elif event.keysym == 'Down':
		canvas_new.move(1, 0, 30)
		canvas_new.itemconfig(1, fill="red", outline="green")
	elif event.keysym == 'Left':
		canvas_new.move(1, -30, 0)
		canvas_new.itemconfig(1, fill="green", outline="red")
	else:
		canvas_new.move(my_triangle, 30, 0)
		canvas_new.itemconfig(1, fill="pink", outline="blue")
#		canvas_new.move(ID=1=my_triangle, X, Y)
canvas_new.bind_all('<KeyPress-Up>',move_triangle)
canvas_new.bind_all('<KeyPress-Down>',move_triangle)
canvas_new.bind_all('<KeyPress-Left>',move_triangle)
canvas_new.bind_all('<KeyPress-Right>',move_triangle)

while True:
	for x in range(0, 60):
		canvas_new.move(1, 5, 5) # move object ID 1 diagonally of 5px,5px (x,y)
		tk_new.update()
		time.sleep(0.05)

	for x in range(0, 60):
		canvas_new.move(1, -5, -5) # move object ID 1 backwards of 5px,5px (x,y)
		tk_new.update()
		time.sleep(0.05)
