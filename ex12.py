#!/usr/bin/env python3
'''
#1:  fill screen with triangles
from tkinter import *
import random
w=400
h=400
tk = Tk()
canvas = Canvas(tk, width=w, height=h)
canvas.pack()

# book solution
colors = ['red', 'green', 'blue', 'yellow', 'orange', 'white', 'purple']
def random_triangle():
	p1 = random.randrange(w)
	p2 = random.randrange(h)
	p3 = random.randrange(w)
	p4 = random.randrange(h)
	p5 = random.randrange(w)
	p6 = random.randrange(h)
	my_color = random.choice(colors)
	canvas.create_polygon(p1, p2, p3, p4, p5, p6, \
			fill=my_color, outline="")

for x in range(0, 200):
	random_triangle()
# end of book solution

# my solution
def random_triangle(width, height, fill_color):
	x1 = random.randrange(width)
	y1 = random.randrange(height)
	x2 = random.randrange(width)
	y2 = random.randrange(height)
	x3 = random.randrange(width)
	y3 = random.randrange(height)
	canvas.create_polygon(x1, y1, x2, y2, x3, y3, fill=fill_color, outline='black')

for x in reversed(range(0, 100)):  # range from 100 to 0
	random_triangle(400, 400, 'green')
	random_triangle(400, 400, 'red')
	random_triangle(400, 400, 'blue')
	random_triangle(400, 400, 'orange')
# end of my solution

#2: moving triangle
import time
from tkinter import *
tk_new = Tk()
canvas_new = Canvas(tk_new, width=420, height=220)
canvas_new.pack()
my_triangle = canvas_new.create_polygon(10, 10, 10, 60, 50, 35)

for x in range(0, 35):
	canvas_new.move(1, 10, 0) # move object ID 1 horiz of 10px (x)
	tk_new.update()
	time.sleep(0.05)

for x in range(0, 14):
	canvas_new.move(1, 0, 10) # move object ID 1 down of 10px (y)
	tk_new.update()
	time.sleep(0.05)
	
	
for x in range(0, 35):
	canvas_new.move(1, -10, 0) # move object ID 1 horiz of 10px (x)
	tk_new.update()
	time.sleep(0.05)

for x in range(0, 14):
	canvas_new.move(1, 0, -10) # move object ID 1 down of 10px (y)
	tk_new.update()
	time.sleep(0.05)
'''
#3: the moving photo
from tkinter import *
import time
tk = Tk()
w = 320
h = 320
canvas = Canvas(tk, width=w, height=h)
canvas.pack()
myimage = PhotoImage(file='./workdir/gb.gif')
canvas.create_image(0, 0, anchor=NW, image=myimage)
# infinite bounce
while True:
	for x in range(0, 30):
		canvas.move(1, 10, 10)
		tk.update()
		time.sleep(0.05)

	for x in range(0, 30):
		canvas.move(1, -10, -10)
		tk.update()
		time.sleep(0.05)
