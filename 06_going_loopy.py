# for loop (0, 1, 2 ,3, 4)
for x in range(0,5):
	print("Hello!")

# [ 10, 11, 12 .. 19 ]
print(list(range(10,20)))

for x in range(0,11):
	print('hello %s' %x)

wizard_list = ['-------------', 'spider legs', 'toe of frog', 'snail tongue', 'bat wing', 'slug butter', 'bear burp']
for i in wizard_list:
	print(i)

new_list = ['eins', 'zwei', 'drei']
for j in new_list:
		print(j)
		for k in new_list:
			print(k)
			
''' golden coins ~ every week loop '''
found_coins = 20
magic_coins_per_week = 70
stolen_coins = 3

number_of_weeks = 5 # change me!
coins = found_coins
#for week in range(1,53):
for week in (range(1, number_of_weeks)):
	coins = coins + magic_coins_per_week - stolen_coins
	print('Week %s = %s' % (week, coins))
	
### p.75 while loop
for step in range(0,21):
	print(step)


step = 0
tired = False
badweather = None
while step < 10001:
	print(step)
	if tired == True:
		break 
	elif badweather == True:
		break 
	else:
		step = step + 1
		
x = 45
y = 80
while x < 50 and y < 100:
	x = x + 1
	y = y + 1
	print(x, y)
