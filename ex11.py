#!/usr/bin/env python
# ex11.py
import turtle
t = turtle.Pen()

print(' #1: Drawing an Octagon ')
def draw_octagon():
	for loop in range(1, 9):
		t.forward(40)
		t.right(45)

draw_octagon()

print('\n #2: Drawing a Filled Octagon')
t.reset()
def filled_octagon(filled):
	if filled == True:
		t.color(1, 1, 0)
		t.begin_fill()
	draw_octagon()
	if filled == True:
		t.end_fill()

filled_octagon(True)
t.color(0, 0, 0)
draw_octagon()

print('\n #3: Another Star-Drawing Function')
t.reset()
def draw_star(size, points):
	angle = 360 / points
	for x in range(0, points):
		t.forward(size)
		t.left(180 - angle)
		t.forward(size)
		t.right(180-(angle *2))
		
draw_star(80, 70)
