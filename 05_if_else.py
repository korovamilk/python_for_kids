# capitolo 5
# if and else

age = 21
msg = ('Your age is %s')
print(msg % age)

# maggiore o uguale a
if age >= 20:
	print('You are too old! ')
# uguale a
if age == 26:
	print('but 26 is not so old.. :)')
# non uguale a
if age != 26:
	print('''You don't have  my age''')

print

# if-then-else statements
age = 9
if age >= 18:
	print('Ok, sei maggiorenne..')
else:
	print('Spiacente, devi essere maggiorenne')

# if and elif (e conversione di stringa in int!)
print
string_age = '10'
age = int(string_age)
if age == 10:
	print('Your age is 10, greetz!')
elif age == 11:
	print('11?')
elif age == 13:
	print('almost 14')
else:
	print('''What's your age?''')

print
# variables with no value (None) --- null?
myval = None
print(myval)

if myval == None:
	print("The variable myval doesn't have a value")
