#!/usr/bin/env python3

print('Creating a clickable button\n')

def hello():
	print('hello there')

# named parameters
def person(width, height):
	print('I am %s feet wide, %s feet high' % (width, height))
	
person(10, 3)

person(height=5, width=10)


from tkinter import *
tk = Tk()
print('Creating a clickable button\n')
btn = Button(tk, text="click me", command=hello)
btn.pack()

print('Creating a Canvas\n')
tk0 = Tk()
canvas0 = Canvas(tk0, width=500, height=500)
canvas0.pack()
canvas0.create_line(0, 0, 500, 500)

# same with turtle
import turtle
turtle.setup(width=500, height=500)
t = turtle.Pen()
t.up()
t.goto(-250, 250)
t.down()
t.goto(500, -500)

# drawing boxes
tk1 = Tk()
canvas1 = Canvas(tk1, width=400, height=400)
canvas1.pack()
# canvas.create_rectangle(x1, y1, x2, y2)
canvas1.create_rectangle(10, 10, 300, 50)
canvas1.create_rectangle(10, 10, 50, 300)
canvas1.create_rectangle(80, 80, 130, 130) # this is a square

# drawing a lot of rectangles
import random
tk2 = Tk()
canvas2 = Canvas(tk2, width = 500, height = 500)
canvas2.pack()

def random_rectangle(width, height, fill_color):
	x1 = random.randrange(width)
	y1 = random.randrange(height)
	x2 = x1 + random.randrange(width)
	y2 = y1 + random.randrange(height)
	canvas2.create_rectangle(x1, y1, x2, y2, fill=fill_color)

for x in range(0, 200):
	random_rectangle(400, 400, 'green')
	random_rectangle(400, 400, 'red')
	random_rectangle(400, 400, 'blue')
	random_rectangle(400, 400, 'orange')
	random_rectangle(400, 400, 'yellow')
	random_rectangle(400, 400, 'pink')
	random_rectangle(400, 400, 'purple')
	random_rectangle(400, 400, 'violet')
	random_rectangle(400, 400, 'magenta')
	random_rectangle(400, 400, 'cyan')
	random_rectangle(400, 400, '#ffd800') # hex
	

print('%05x' % 65535) # prints out 5 digits
# ie. 0ffff

''' not working
from tkinter import *
colorchooser.askcolor()
'''	

