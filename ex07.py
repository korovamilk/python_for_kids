#!/usr/bin/env python
print('#1: Basic Moon Weight Function\n')

def moon_weight(kg, increase):
	print('Kg: %s ~ Perc: %s' % (kg, increase))
	years = 1
	while years < 16:
		kg += increase
		m_weight = kg * 0.165
		print('Year: %s ~ Moon Weight: %s' % (years, m_weight))
		years += 1
		
moon_weight(40, 0.5)

print('#2: Moon Weight Function and Years\n')		 
def moon_weight_years(kg, increase, y_step):
	# print('Kg: %s ~ Perc: %s ~ YearStep: %s' % (kg, perc, y_step))
	y_step += 1 
	for years in range (1, y_step):
		kg = kg + increase
		m_weight = kg * 0.165
		print('\nYear: %s ~ Moon Weight: %s' % (years, m_weight))

moon_weight_years(35, 0.30, 5)


print('#3: Moon Weight Program\n')

import sys
def moon_weight_noparams():
	print('\nPlease enter your current Earth weight:' )
	earth_kg = float(sys.stdin.readline())

	print('\nPlease enter the amount your weight might increase each year:' )
	perc_inc = float(sys.stdin.readline())

	print('\nPlease enter the number of years:' )
	numb_years = int(sys.stdin.readline())
	numb_years += 1

	for numb_years in range (1, numb_years):
		earth_kg += perc_inc
		moon_weight = earth_kg * 0.165
		print('Year %s is %s' % (numb_years, moon_weight))
		
moon_weight_noparams()
