# python for kids
# capitolo 4 - disegnare con il modulo turtle

import turtle

''' 1 '''
''' rettangolo'''
rettangolo = turtle.Pen()
rettangolo.forward(120)
rettangolo.left(90)
rettangolo.forward(50)
rettangolo.left(90)
rettangolo.forward(120)
rettangolo.left(90)
rettangolo.forward(50)
rettangolo.left(90)
rettangolo.reset()


''' 2 '''
''' triangolo '''
triangolo = turtle.Pen()
triangolo.forward(200)
triangolo.left(90)
triangolo.left(45)
triangolo.forward(140)
triangolo.left(90)
triangolo.forward(140)
triangolo.left(90)
triangolo.left(45)
triangolo.reset()

''' 3 '''
''' box without corners '''
box = turtle.Pen()
box.forward(50)
box.up()
box.forward(25)
box.left(90)
# lato (100px)
box.forward(25)
box.down()
box.forward(50)
box.up()
box.forward(25)
box.left(90)
# lato (100px)
box.forward(25)
box.down()
box.forward(50)
box.up()
box.forward(25)
box.left(90)
# lato (100px)
box.forward(25)
box.down()
box.forward(50)
box.up()
box.forward(25)
box.left(90)
# return
box.forward(25)

