#!/usr/bin/env python
''' 1 '''
new_line = '_______________\n'
print("* the hello loop\n\n")
for x in range(0,20):
	print('hello %s' % x)
	if x < 9:
		break
print(new_line)
print("\n* even numbers")
numeri = 0
while numeri <= 36:
	print numeri
	numeri += 2
	
print("\n* even numbers - alternate version")
for age in range(2, 38, 2):
	print(age)
print(new_line)	

print("five fav ingredients")
ingredients = ['tomato', 'cheese', 'pepper', 'salt', 'oil']
for i in range(0, 5):
	print(i+1, '%s' % ingredients[i])

print(new_line)	

print("five fav ingredients - alt version")
x = 1
while x < 6:
	print('%s %s' % (x,ingredients[x-1]))
	x += 1

print(new_line)	

print("your weight on the moon year by year for the next 15ys")
# weight on moon = your_weight * 0.165 (16.5% of weight on earth)
weight = 64
years = 0
for years in range(1,16):
	weight += 1
	moon_weight = (weight * 0.165) 
	print('Year: %s - My Weight on Earth: %s - My Weight on the moon: %s' % (years, weight, moon_weight))


