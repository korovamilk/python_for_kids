#!/usr/bin/env python
# built-in functions

print('\n1 - absolute value')
print(abs(10))
print(abs(-10))
# is it moving or not?
steps = -3
if abs(steps)> 0:
	print('Yes, he is absolutely moving!')
	
print('\n2 - Boolean function')
print(bool(0)) # returns False
print(bool(-2453467)) # any other number returns True
print(bool("a")) # True
print(bool(' ')) # True!
print(bool(None)) # False if there's no value for string
my_empty_list = []
print(bool(my_empty_list)) # False because it's empty
my_silly_list = ['s', 'i', 'l', 'l', 'y']
print(bool(my_silly_list)) # False if there's no value for string

''' year = input("Year of birth: ")
if not bool(year.rstrip()):
	print("You need to enter a value for your year of birth") '''

print('\n3 - Dir function')
my_string = "pepper"
print(dir(my_string)) # shows all functions available for item 'my_string'
new_string = my_string.upper()
print(my_string)
print(new_string)

print('\n4 - Eval(uate) func')
''' your_calc = input('Enter a calculation: ')
eval(your_calc) '''

print('\n5 - Exec function')
my_small_program = '''print('ham')
print('sandwich')'''
exec(my_small_program)

print('\n6 - Float')
floating = float('12')
print(floating)
'''
your_age = input('Enter your age: ')
age = float(your_age)
fix_age = 18
if age > fix_age:
	print('You are %s years too old' % (age - fix_age))
'''

print('\n7 - Int')
print(int(123.42))

print('\n8 - Len')
word = 'Hallo Leute!'
print(len(word)) # string length

short_list = ['1', '2']
print(len(short_list)) # list length

short_map = {'Me' : '29', 'You' : '40', 'She' : '28'}
print(len(short_map)) # string length

fruit = ['apple', 'banana', 'pear', 'strawberry', 'kiwi']
length = len(fruit)
for x in range(0, length):
	print('the fruit at index %s is %s' % (x, fruit[x]))
	
print('\n9 - Max and Min')
numbers = [4, 12, -20, 8]
print('max: %s ' % max(numbers))
print('min: %s ' % min(numbers))
letters = 's,t,r,i,n,g,s,S,T,R,I,N,G,S'
print('max: %s ' % max(letters))
print('min: %s ' % min(letters))

print('Max: %s' %max(10, 300, 450, 50, 90))
print('Min: %s' %min(10, 300, 450, 50, 90))

print('\n10 - Range function')
x = range(0, 5) #return an 'iterator'
print(x)

# third parameter of range is STEP
y = list(range(0, 100, 33))
print(y)

count_down_by_2 = list(range(40, 10, -2))
print(count_down_by_2)

print('\n11 - Sum')
my_list_of_numbers = list(range(0, 500, 50))
print(my_list_of_numbers)
print(sum(my_list_of_numbers))


print('\n12 - Working with files')

print('Reading a file')

working_with_file = open('./test_file.txt')
show_content = working_with_file.read()
print(show_content)

'''
filename = 'test_file.txt'
test_file = open(filename)
text = test_file.read()
print(text)
'''

print('Writing')
test_file = open('./test_file.txt', 'a') # append mode
test_file.write('This is my test file.  ')
test_file.close()

test_file = open('./test_file.txt')
text = test_file.read()
print(text)
