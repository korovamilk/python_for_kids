# python for kids
# capitolo 3
''' 1 '''
''' favorites '''
# list of hobbies 
games = ['Call of Duty', 'Badminton', 'Darts', 'Dog']
# list of fav foods
food = ['Pizza', 'Salsiccia', 'Bratwurst', 'Pommes Frites', 'Beer']

# join above lists and print resulting variable
favorites = games + food
print(favorites)
print

''' 2 '''
''' counting combatans '''
how_many_combatants = ((3*25)+(2*40))
how_many_mesg = ('Combatants: %s')
print(how_many_mesg % how_many_combatants)
# python shell one liner:
print((3*25)+(2*40))
print

''' 3 '''
''' greetings '''
nome = 'Nome'
cognome = 'Cognome'
greet = ('Hello %s %s!')
print(greet % (nome, cognome))

